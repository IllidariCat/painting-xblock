import Vue from 'vue'
Vue.config.productionTip = false;
/*
import wrap from '@vue/web-component-wrapper';

import Main from './components/Main';
import Editor from './components/Editor';

const WrappedMain = wrap(Vue, Main);
const WrappedEditor = wrap(Vue, Editor);

window.customElements.define('painting-main', WrappedMain);
window.customElements.define('painting-editor', WrappedEditor);
*/

import App from './App';

new Vue({
	render: (h) => h(App)
}).$mount('#app');
