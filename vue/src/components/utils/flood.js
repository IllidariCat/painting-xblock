/**
 * RGBa Color object
 * @typedef {{r: number, g: number, b: number, a: number|undefined}} Color
 * 2D position object
 * @typedef {{x: number, y: number}} Position
 * @module floodFill
 */
/**
 *
 * @param {Color} a
 * @param {Color} b
 * @returns {boolean} - Is colors equal
 */
const matchColor = (a, b) => {
	return a.r === b.r && a.g === b.g && a.b === b.b;
};
/**
 * @description
 * Flood fills closed area withing given canvas context with selected color
 * If area isn't close, changes all pixels with color equals to start points color
 * Function uses "The Holy Grail" algorithm
 * @see {@link http://www.williammalone.com/articles/html5-canvas-javascript-paint-bucket-tool/|Explanation}
 * @param {CanvasRenderingContext2D} context - Canvas 2D context
 * @param {Position} start - Start point
 * @param {Color} fillColor - Color to fill area with
 */
module.exports = function floodFill(context, start, fillColor) {
	const { width, height } = context.canvas;
	const image = context.getImageData(0, 0, width, height);
	const step = width * 4;

	/**
	 * @description
	 * Returns color of pixel at given position
	 * @param {number} imagePosition - Start point of pixel color definition in ImageData.data array
	 * @returns {Color}
	 */
	const getColor = (imagePosition) => ({
		r: image.data[imagePosition],
		g: image.data[imagePosition + 1],
		b: image.data[imagePosition + 2],
		a: image.data[imagePosition + 3],
	});

	/**
	 * @description
	 * Sets color of pixel at given position
	 * @param {number} imagePosition - Start point of pixel color definition in ImageData.data array
	 * @param {Color} color - New pixel color
	 */
	const setColor = (imagePosition, color) => {
		image.data[imagePosition] = color.r;
		image.data[imagePosition + 1] = color.g;
		image.data[imagePosition + 2] = color.b;
		image.data[imagePosition + 3] = (color.a === undefined) ? 255: color.a;
	};

	/**
	 * @description
	 * Transforms {Position} type into index points to beginning of pixel definition in ImageData.data array
	 * @param {Position} position - 2D pixel coordinates from top-left corner
	 * @returns {number} - Transformed position
	 */
	const getImagePosition = ({x, y}) => {
		return (y * width + x) * 4;
	};

	const color = getColor(getImagePosition(start));

	//If we skip this condition and colors will be equal we'll get an infinite loop.
	//This happens because we fill with same color as trying to replace.
	if (matchColor(color, fillColor)) {
		return
	}

	const stack = [start];

	while(stack.length) {
		const coords = stack.pop();
		let {x, y} = coords;
		let position = getImagePosition(coords);

		while (y >= 0 && matchColor(color, getColor(position))) {
			y -= 1;
			position -= step;
		}

		position += step;
		y += 1;
		let reached = { left: false, right: false };

		while (y < height && matchColor(color, getColor(position))) {
			y += 1;
			setColor(position, fillColor);
			if (x > 0) {
				if (matchColor(color, getColor(position - 4))) {
					if (!reached.left) {
						stack.push({ x: x - 1, y });
						reached.left = true
					}
				} else if (reached.left) {
					reached.left = false;
				}
			}
			if (x < width) {
				if (matchColor(color, getColor(position + 4))) {
					if (!reached.right) {
						stack.push({ x: x + 1, y });
						reached.right = true;
					}
				} else if (reached.left) {
					reached.right = false;
				}
			}
			position += step;
		}
	}
	context.putImageData(image, 0, 0)
};