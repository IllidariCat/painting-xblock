module.exports = {
	data: () => ({
		cookies: {}
	}),
	mounted() {
		this.cookies = document.cookie.split(";").reduce((o, c) => {
      const [key, value] = c.split('=');
      o[key.trim()] = value;
      return o
    }, {})
	}
};