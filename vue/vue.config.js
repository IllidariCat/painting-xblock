const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
	css: { extract: false },
	outputDir: path.resolve(__dirname, "../painting/public/js/src/"),
	productionSourceMap: false,
	configureWebpack: {
		externals: {
			DrawingBoard: "DrawingBoard",
			jquery: "jQuery"
		},
		plugins: [
			new CopyPlugin([
				{ from: 'public/init.js', to: `painting_student.js` },
				{ from: 'public/init_editor.js', to: `painting_editor.js` },
			]),
		],
	},
};
