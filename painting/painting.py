"""TO-DO: Write a description of what this XBlock is."""

import pkg_resources
from web_fragments.fragment import Fragment
from xblock.core import XBlock
from xblock.fields import Integer, String, Scope
from xblockutils.resources import ResourceLoader

loader = ResourceLoader(__name__)


class PaintingXBlock(XBlock):
    """
    TO-DO: document what your XBlock does.
    """

    # Fields are defined on the class.  You can access them in your code as
    # self.<fieldname>.
    painting = String(
        default="", scope=Scope.user_state,
        help="Base64 encoded users painted image"
    )
    display_name = String(
        display_name = "Display Name",
        default="XBlock Name",
        scopde=Scope.settings
    )
    width = Integer(
        default=800, scope=Scope.settings,
        help="Canvas width",
    )
    height = Integer(
        default=600, scope=Scope.settings,
        help="Canvas height"
    )
    minbrushsize = Integer(
        scope=Scope.settings,
        default=1, help="Minimal brush size in pixels",
    )
    maxbrushsize = Integer(
        scope=Scope.settings,
        default=64, help="Maximal brush size in pixels",
    )
    brushsizestep = Integer(
        scope=Scope.settings,
        default=1, help="Brush size changing step in pixels",
    )

    def resource_string(self, path):
        """Handy helper for getting resources from our kit."""
        data = pkg_resources.resource_string(__name__, path)
        return data.decode("utf8")

    # TO-DO: change this view to display your data your own way.
    def student_view(self, context=None):
        """
        The primary view of the RobboTestXBlock, shown to students
        when viewing courses.
        """
        frag = Fragment()
        frag.add_content(
            loader.render_django_template(
                '/public/html/painting.html',
                context={'self': self},
            )
        )
        frag.add_javascript(self.resource_string("public/js/src/painting.min.js"))
        frag.add_javascript(self.resource_string("public/js/src/painting_student.js"))
        frag.initialize_js('PaintingXBlock')
        return frag

    def studio_view(self, context=None):
        """
        The primary view of the TestXBlock, shown to students
        when viewing courses.
        """
        frag = Fragment()
        frag.add_content(
            loader.render_django_template(
                '/public/html/painting_editor.html',
                context={'self': self},
            )
        )
        frag.add_javascript(self.resource_string("public/js/src/painting.min.js"))
        frag.add_javascript(self.resource_string("public/js/src/painting_editor.js"))
        frag.initialize_js('PaintingXBlockEditor')
        return frag

    # TO-DO: change this handler to perform your own actions.  You may need more
    # than one handler, or you may not need any handlers at all.
    @XBlock.json_handler
    def student_submit(self, data, suffix=''):
        self.painting = data['image'];
        return

    @XBlock.json_handler
    def studio_submit(self, data, suffix=''):
        fields = ['width', 'height', 'minbrushsize', 'maxbrushsize', 'brushsizestep']
        for field in fields:
            setattr(self, field, data[field])

        return data;

    # TO-DO: change this to create the scenarios you'd like to see in the
    # workbench while developing your XBlock.
    @staticmethod
    def workbench_scenarios():
        """A canned scenario for display in the workbench."""
        return [
            ("PaintingXBlock",
             """<painting/>
             """),
            ("Multiple PaintingXBlock",
             """<vertical_demo>
                <painting/>
                <painting/>
                <painting/>
                </vertical_demo>
             """),
        ]
